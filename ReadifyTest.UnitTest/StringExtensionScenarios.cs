﻿using System.Threading.Tasks;
using NUnit.Framework;
using ReadifyTest.Extensions;
using Shouldly;
using TestStack.BDDfy;

namespace ReadifyTest.UnitTest
{
    [TestFixture]
    public class StringExtensionScenarios
    {
        private string _stringToReverse;
        private string _result;

        [TestCase("abcdefg", "gfedcba")]
        [TestCase("123456", "654321")]
        [TestCase("", "")]
        [TestCase(null, null)]
        public void ShouldBeAbleToReverseAString(string source, string expected)
        {
            this.Given(x => x.GivenASentence(source))
                .When(x => x.WhenReversingTheSentence())
                .Then(x => x.ThenTheReversedStringShouldBeAsExpected(expected))
                .BDDfy();
        }

        private void GivenASentence(string sentence)
        {
            _stringToReverse = sentence;
        }

        private void WhenReversingTheSentence()
        {
            _result = _stringToReverse.ReverseWords();
        }

        private void ThenTheReversedStringShouldBeAsExpected(string expected)
        {
            _result.ShouldBe(expected);
        }
    }
}
