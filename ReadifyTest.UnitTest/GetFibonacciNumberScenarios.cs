﻿using System;
using NUnit.Framework;
using ReadifyTest.Services;
using Shouldly;
using TestStack.BDDfy;

namespace ReadifyTest.UnitTest
{
    [TestFixture]
    public class GetFibonacciNumberScenarios
    {
        private IFibonacciService _service;
        private int _sequence;
        private long _result;
        private Exception _expectedException;

        [SetUp]
        public void Setup()
        {
            _service = new FibonacciService();
        }

        [TestCase(0, 0L)]
        [TestCase(1, 1L)]
        [TestCase(2, 1L)]
        [TestCase(3, 2L)]
        [TestCase(6, 8L)]
        [TestCase(94, 1293530146158671551L)]
        public void ShouldGetFibonacciNumber(int sequence, long fibonacciNumber)
        {
            this.Given(x => x.GivenASequenceNumber(sequence))
                .When(x => x.WhenGettingTheFibonacciNumber())
                .Then(x => x.ThenTheFibonacciNumberShouldBeAsExpected(fibonacciNumber))
                .BDDfy();
        }

        [Test]
        public void ShouldThrowExceptionWhenSequenceNumberIsNegative()
        {
            this.Given(x => x.GivenANegativeSequenceNumber())
                .When(x => x.WhenGettingTheFibonacciNumberWithExpectedException())
                .Then(x => x.ThenTheExceptionShouldBeSequenceShouldNotBeNegative())
                .BDDfy();

        }

        [Test]
        public void ShouldThrowExceptionWhenSequenceNumberIsTooLarge()
        {
            this.Given(x => x.GivenTooLargeSequenceNumber())
                .When(x => x.WhenGettingTheFibonacciNumberWithExpectedException())
                .Then(x => x.ThenTheExceptionShouldBeSequenceTooLarge())
                .BDDfy();
        }

        private void GivenASequenceNumber(int sequence)
        {
            _sequence = sequence;
        }

        private void GivenANegativeSequenceNumber()
        {
            _sequence = -1;
        }

        private void GivenTooLargeSequenceNumber()
        {
            _sequence = 100;
        }

        private void WhenGettingTheFibonacciNumber()
        {
            _result = _service.GetFibonacciNumberAtSequence(_sequence);
        }

        private void WhenGettingTheFibonacciNumberWithExpectedException()
        {
            try
            {
                _result = _service.GetFibonacciNumberAtSequence(_sequence);
            }
            catch (Exception ex)
            {
                _expectedException = ex;
            }
        }

        private void ThenTheFibonacciNumberShouldBeAsExpected(long expected)
        {
            _result.ShouldBe(expected);
        }

        private void ThenTheExceptionShouldBeSequenceShouldNotBeNegative()
        {
            _expectedException.ShouldBeOfType<ArgumentException>();
            _expectedException.Message.ShouldBe(ReadifyResource.Message.FibonacciSequenceShouldNotBeNegative);
        }

        private void ThenTheExceptionShouldBeSequenceTooLarge()
        {
            _expectedException.ShouldBeOfType<ArgumentException>();
            _expectedException.Message.ShouldBe(ReadifyResource.Message.TooLargeSequence);
        }
    }
}
