﻿using NUnit.Framework;
using ReadifyTest.Validators;
using Shouldly;
using TestStack.BDDfy;

namespace ReadifyTest.UnitTest.Validators
{
    [TestFixture()]
    public class AcuteTriangleValidatorScenario
    {
        private int _a, _b, _c;
        private string _validatedResult;
        private ITriangleTypeValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new AcuteTriangleValidator();
        }

        [TestCase(5, 4, 6, "Acute")]
        [TestCase(4, 3, 5, "")]
        public void ShouldBeAbleToValidate(int a, int b, int c, string expectedType)
        {
            this.Given(x => x.GiveThreeLines(a, b, c))
                .When(x => x.WhenValidating())
                .Then(x => x.ThenTheResultShouldBeAsExpected(expectedType))
                .BDDfy();
        }

        private void GiveThreeLines(int a, int b, int c)
        {
            _a = a;
            _b = b;
            _c = c;
        }

        private void WhenValidating()
        {
            _validatedResult = _validator.Validate(_a, _b, _c);
        }

        private void ThenTheResultShouldBeAsExpected(string expected)
        {
            _validatedResult.ShouldBe(expected);
        }
    }
}
