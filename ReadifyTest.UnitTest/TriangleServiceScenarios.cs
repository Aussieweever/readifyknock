﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ReadifyTest.Services;
using ReadifyTest.Validators;
using Shouldly;
using TestStack.BDDfy;

namespace ReadifyTest.UnitTest
{
    [TestFixture]
    public class TriangleServiceScenarios
    {
        private ITriangleService _service;
        private int _a, _b, _c;
        private int[] _actualLinesArray;
        private string _actualTriangleTypes;
        private Exception _expectedException;

        [SetUp]
        public void Setup()
        {
            var validators = new List<ITriangleTypeValidator>
            {
                new AcuteTriangleValidator(),
                new EquilateralTriangleValidator(),
                new IsoscelesTriangleValidator(),                
                new ObtuseTriangleValidator(),
                new RightTriangleValidator(),
                new ScaleneTriangleValidator()
            };

            _service = new TriangleService(validators);
        }

        [TestCase(3, 4, 5, new[] {3, 4, 5})]
        [TestCase(4, 5, 3, new[] {3, 4, 5})]
        [TestCase(5, 3, 4, new[] {3, 4, 5})]
        [TestCase(6, 6, 5, new[] {5, 6, 6})]
        [TestCase(6, 6, 6, new[] {6, 6, 6})]
        public void ShouldSortLinesByLength(int a, int b, int c, int[] expected)
        {
            this.Given(x => x.GivenThreeLines(a, b, c))
                .When(x => x.WhenSortingLines())
                .Then(x => x.ThenTheLinesShouldBeSortedByLength(expected))
                .BDDfy();
        }

        [TestCase(0, 1, 2)]
        [TestCase(-1, 1, 2)]
        public void ShouldThrowExceptionWhenLineLengthIsNotPositive(int a, int b, int c)
        {
            this.Given(x => x.GivenThreeLines(a, b, c))
                .When(x => x.WhenValidatingWithExpectedError())
                .Then(x => x.ThenTheExceptionShouldBeInvalidTriangleLineLength())
                .BDDfy();
        }

        [TestCase(1, 1, 10)]
        [TestCase(1, 2, 3)]        
        public void ShouldThrowExceptionWhenNotATriangle(int a, int b, int c)
        {
            this.Given(x => x.GivenThreeLines(a, b, c))
                .When(x => x.WhenValidatingWithExpectedError())
                .Then(x => x.ThenTheExceptionShouldBeNotATriangle())
                .BDDfy();
        }

        [TestCase(5, 4, 6, "Acute,Scalene")]
        [TestCase(6, 6, 6, "Equilateral,Acute")]
        [TestCase(3, 4, 6, "Obtuse,Scalene")]
        [TestCase(3, 5, 4, "Right,Scalene")]
        public void ShouldGetCorrectTriangleTypes(int a, int b, int c, string expectedType)
        {
            this.When(x => x.WhenValidatingTriangleType(a, b, c))
                .Then(x => x.ThenTheTriangleTypeShouldBe(expectedType))
                .BDDfy();
        }

        private void GivenThreeLines(int a, int b, int c)
        {
            _a = a;
            _b = b;
            _c = c;
        }

        private void WhenSortingLines()
        {
            _actualLinesArray = _service.SortLines(_a, _b, _c);
        }

        private void WhenValidatingWithExpectedError()
        {
            try
            {
                _actualLinesArray = _service.SortLines(_a, _b, _c);
            }
            catch (Exception ex)
            {
                _expectedException = ex;
            }
        }

        private void WhenValidatingTriangleType(int a, int b, int c)
        {
            _actualTriangleTypes = _service.ValidateTriangleType(a, b, c);
        }

        private void ThenTheLinesShouldBeSortedByLength(int[] expected)
        {
            _actualLinesArray.Length.ShouldBe(expected.Length);
            for (var i = 0; i < 3; ++i)
            {
                _actualLinesArray[i].ShouldBe(expected[i]);
            }
        }

        private void ThenTheExceptionShouldBeInvalidTriangleLineLength()
        {
            _expectedException.ShouldBeOfType(typeof(ArgumentException));
            _expectedException.Message.ShouldBe(ReadifyResource.Message.InvalidTriangleLineLength);
        }

        private void ThenTheExceptionShouldBeNotATriangle()
        {
            _expectedException.ShouldBeOfType(typeof(ArgumentException));
            _expectedException.Message.ShouldBe(ReadifyResource.Message.IsNotATriangle);
        }

        private void ThenTheTriangleTypeShouldBe(string expected)
        {
            var actualTypes = _actualTriangleTypes.Split(',');
            var expectedTypes = expected.Split(',');

            actualTypes.Length.ShouldBe(expectedTypes.Length);
            foreach (var type in expectedTypes)
            {
                var existingType =
                    actualTypes.FirstOrDefault(x => x.Equals(type, StringComparison.InvariantCultureIgnoreCase));
                existingType.ShouldNotBeNull();

            }
        }
    }
}
