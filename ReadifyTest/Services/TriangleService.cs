﻿using System;
using System.Collections.Generic;
using System.Linq;
using ReadifyTest.Validators;


namespace ReadifyTest.Services
{
    public interface ITriangleService
    {
        int[] SortLines(int a, int b, int c);
        string ValidateTriangleType(int a, int b, int c);
    }

    public class TriangleService : ITriangleService
    {
        private IEnumerable<ITriangleTypeValidator> _triangleTypeValidators;

        public TriangleService(IEnumerable<ITriangleTypeValidator> triangleTypeValidators)
        {
            _triangleTypeValidators = triangleTypeValidators;
        }

        public int[] SortLines(int a, int b, int c)
        {
            var list = new List<int> {a, b, c};
            if (list.Any(x => x <= 0))
                throw new ArgumentException(ReadifyResource.Message.InvalidTriangleLineLength);

            if ((a >= b + c) || (b >= a + c) || (c >= a + b))
                throw new ArgumentException(ReadifyResource.Message.IsNotATriangle);

            if ((a <= Math.Abs(c - b)) || (b <= Math.Abs(a - c)) || (c <= Math.Abs(a - b)))
                throw new ArgumentException(ReadifyResource.Message.IsNotATriangle);

            return list.OrderBy(x => x).ToArray();
        }

        public string ValidateTriangleType(int a, int b, int c)
        {
            var lines = SortLines(a, b, c);
            var types = new List<string>();

            foreach (var validator in _triangleTypeValidators)
            {
                var type = validator.Validate(lines[0], lines[1], lines[2]);
                if (string.IsNullOrWhiteSpace(type))
                    continue;

                types.Add(type);
            }

            return string.Join(",", types);
        }
    }
}
