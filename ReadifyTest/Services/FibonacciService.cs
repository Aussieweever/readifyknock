﻿using System;

namespace ReadifyTest.Services
{
    public interface IFibonacciService
    {
        long GetFibonacciNumberAtSequence(int sequence);
    }

    public class FibonacciService : IFibonacciService
    {
        public long GetFibonacciNumberAtSequence(int sequence)
        {
            if(sequence < 0)
                throw new ArgumentException(ReadifyResource.Message.FibonacciSequenceShouldNotBeNegative);

            if(sequence > 94)
                throw new ArgumentException(ReadifyResource.Message.TooLargeSequence);

            if (sequence < 2)
                return sequence;

            long a = 0, b = 1, c = 0;
            
            for (int i = 2; i <= sequence; ++i)
            {
                c = a + b;
                a = b;
                b = c;
            }

            return c;
        }
    }
}
