﻿namespace ReadifyTest.Validators
{

    public interface ITriangleTypeValidator
    {
        string Validate(int a, int b, int c);
    }
}
