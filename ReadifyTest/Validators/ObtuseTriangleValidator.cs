﻿using System;

namespace ReadifyTest.Validators
{
    public class ObtuseTriangleValidator : ITriangleTypeValidator
    {
        public string Validate(int a, int b, int c)
        {
            if (Math.Pow(a, 2) + Math.Pow(b, 2) < Math.Pow(c, 2))
                return ReadifyResource.Message.ObtuseTriangle;

            return string.Empty;
        }
    }
}
