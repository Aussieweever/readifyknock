﻿namespace ReadifyTest.Validators
{
    public class EquilateralTriangleValidator : ITriangleTypeValidator
    {
        public string Validate(int a, int b, int c)
        {
            if (a == b && b == c)
                return ReadifyResource.Message.EquilateralTriangle;

            return string.Empty;
        }
    }
}
