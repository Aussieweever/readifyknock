﻿using System;

namespace ReadifyTest.Validators
{
    public class IsoscelesTriangleValidator : ITriangleTypeValidator
    {
        public string Validate(int a, int b, int c)
        {
            if ((a == b && a != c) || (a == c && a != b) || (b == c && b != a))
                return ReadifyResource.Message.IsoscelesTriangle;

            return String.Empty;            
        }
    }
}
