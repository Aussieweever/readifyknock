﻿namespace ReadifyTest.Validators
{
    public class ScaleneTriangleValidator : ITriangleTypeValidator
    {
        public string Validate(int a, int b, int c)
        {
            if (a != b && a != c && b != c)
                return ReadifyResource.Message.ScaleneTriangle;

            return string.Empty;
        }
    }
}
