﻿using System;
using System.Linq;

namespace ReadifyTest.Extensions
{
    public static class StringExtension
    {
        public static string ReverseWords(this string sentence)
        {
            if (sentence == null)
                return null;

            return new String(sentence.ToCharArray().Reverse().ToArray());
        }
    }
}
