﻿using Autofac;
using ReadifyTest.Extensions;
using ReadifyTest.Services;
using ReadifyTest.Validators;
using Module = Autofac.Module;

namespace ReadifyTest
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FibonacciService>().As<IFibonacciService>();
            builder.RegisterType<TriangleService>().As<ITriangleService>();

            builder.RegisterType<AcuteTriangleValidator>().As<ITriangleTypeValidator>();
            builder.RegisterType<EquilateralTriangleValidator>().As<ITriangleTypeValidator>();
            builder.RegisterType<IsoscelesTriangleValidator>().As<ITriangleTypeValidator>();
            builder.RegisterType<ObtuseTriangleValidator>().As<ITriangleTypeValidator>();
            builder.RegisterType<RightTriangleValidator>().As<ITriangleTypeValidator>();
            builder.RegisterType<ScaleneTriangleValidator>().As<ITriangleTypeValidator>();
        }
    }
}
