﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReadifyTest.Services;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ReadifyTest.Controllers
{
    [Produces("application/json")]
    [Route("api/Fibonacci")]
    public class FibonacciController : Controller
    {
        private readonly IFibonacciService _fibonacciService;

        public FibonacciController(IFibonacciService fibonacciService)
        {
            _fibonacciService = fibonacciService;
        }

        [HttpGet]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(long), "Fibonacci Number")]
        [SwaggerResponse((int) HttpStatusCode.BadRequest, null, "Invalid sequence number")]
        public async Task<IActionResult> GetFibonacciNumber(int n)
        {
            try
            {
                return await Task.Run(() => Ok(_fibonacciService.GetFibonacciNumberAtSequence(n)));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }            
        }
    }
}