﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ReadifyTest.Services;

namespace ReadifyTest.Controllers
{
    [Produces("application/json")]
    [Route("api/TriangleType")]
    public class TriangleTypeController : Controller
    {
        private ITriangleService _triangleService;        

        public TriangleTypeController(ITriangleService triangleService)
        {
            _triangleService = triangleService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTTriangleType(int a, int b, int c)
        {
            try
            {
                return Ok(_triangleService.ValidateTriangleType(a, b, c));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            
        }
    }
}