﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ReadifyTest.Controllers
{
    [Route("api/Token")]
    [Produces("application/json")]
    public class TokenController : Controller
    {
        private readonly IConfiguration _configuration;

        public TokenController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Get Readify Token
        /// </summary>
        /// <returns>The predefined Readify token</returns>
        [HttpGet]
        [Route("")]
        [SwaggerResponse(200, typeof(string), "Get Readify Token")]
        public async Task<string> GetReadifyToken()
        {
            //return _configuration.GetValue<string>("ReadifyToken");
            return await Task.Run(() => _configuration.GetValue<string>("ReadifyToken"));
        }

    }
}