﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ReadifyTest.Extensions;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ReadifyTest.Controllers
{
    [Produces("application/json")]
    [Route("api/ReverseWords")]
    public class ReverseWordsController : Controller
    {
        /// <summary>
        /// Reverses the letters of each word in a sentence.
        /// </summary>
        /// <param name="sentence">A sentence</param>
        /// <returns>Reversed string</returns>
        [HttpGet]
        [Route("")]
        [SwaggerResponse(200, typeof(string), "Reversed string")]
        public async Task<string> ReverseString(string sentence)
        {
            return await Task.Run(() => sentence.ReverseWords());
        }
    }
}